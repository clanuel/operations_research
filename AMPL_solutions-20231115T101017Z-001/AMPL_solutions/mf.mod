set N; # set of nodes;
set A within {N,N}; # set of arcs;

param s symbolic in N; # source node
param t symbolic in N; # destination node

param u{A} >=0; # arc capacity

var x{(i,j) in A} >=0; # flow on arc (i,j)
var v; # flow from s to t

maximize flow: v;

subject to source: -sum{(s,j) in A} x[s,j] = -v;

subject to destination: sum{(i,t) in A} x[i,t] = v;

subject to balance{i in N: i <>s and i <>t}: 
		sum{(j,i) in A}x[j,i] - sum{(i,j) in A} x[i,j]  = 0;
		
subject to capacity{(i,j) in A}: x[i,j] <= u[i,j];