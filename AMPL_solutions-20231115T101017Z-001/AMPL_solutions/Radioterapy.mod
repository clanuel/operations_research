var x1 >=0; #beam 1 qt (kilorad)
var x2 >=0; #beam 2 qt (kilorad)


minimize healthy_anatomy: 0.4*x1+0.5*x2; 
#total radiation on healthy anatomy must be reduced

s.t. critical: 0.3*x1+0.1*x2 <=2.7; 
#critical tissue must not radiated more then 2.7 kilorad

s.t. tumor_region:0.5*x1+0.5*x2=6; 
#tumor region must be radiated with 6 kilorad
 
s.t. center: 0.6*x1+0.4*x2 >=6; 
#the center of the tumor must be radiated at least with 6 kilorad

