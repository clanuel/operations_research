set C;
set S ;

param p{C,S};
param c{S};
param budget;

var y{S} binary;
var x{C,S} binary;

var z;

minimize max_power: z;

s.t. max_value{i in C, j in S}: z>=p[i,j]*x[i,j];

s.t. budget_constraint: 
	sum{j in S} c[j]*y[j]<=budget;

s.t. assignment{i in C}: sum{j in S} x[i,j] = 1;

s.t. consistency{i in C, j in S}: x[i,j] <= y[j];

