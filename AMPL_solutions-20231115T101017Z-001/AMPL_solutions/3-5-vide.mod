set N; # set of nodes;
set A within {N,N}; # set of arcs;

param s symbolic in N; # source node
param t symbolic in N; # destination node

param u{A} >=0; # arc capacity

