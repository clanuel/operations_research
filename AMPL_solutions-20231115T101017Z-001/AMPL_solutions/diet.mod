############# model file #############
set F; #food
set N; #nutrient

param a{F,N}; #qt of nutrient for each serving of given food
param cost{F};   #cost of serving of given food
param Fmax{F};   #maximum amount of a given food

##putting a default values allows 
##to avoid defining all the parameters in the dat file
param Nmin{j in N}, default 0;
param Nmax{j in N}, default 10000;


var x{i in F} >= 0, <=Fmax[i];

minimize total_cost: sum {i in F} cost[i]*x[i];

subject to nutritional_reqs_min{j in N}:
sum {i in F} a[i,j]*x[i] >= Nmin[j];

subject to nutritional_reqs_max{j in N}:
 sum {i in F} a[i,j]*x[i] <= Nmax[j];

