set I; # set of investments

param w{I} >=0;
param p{I} >= 0;

param budget;

var x{I}, >=0, <=1; 

maximize profit: sum{i in I} p[i]*x[i];

s.t. budget_constraint: sum{i in I} w[i]*x[i] <=budget;

