set R; #set of raw materials

set N;# set of nutrients

param contents{R,N};

param need{N};

param c{R};

var x{R} >=0; # hg of raw material in 1 hf of feed

minimize cost: sum{i in R} c[i]*x[i];

s.t. minimum_amount{j in N}:
sum{i in R} contents[i,j]*x[i] >= need[j];

s.t. amount_of_feed: sum{i in R} x[i] =1;