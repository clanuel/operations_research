set Beams; #available beams
set HealtyCells; #regions/groups of healty cells
set TumorCells; #regions of tumor cells
set CenterCells; #regions of center of tumor
set CriticalCells; # regions of critical cells

#set Cells= 
#HealtyCells union TumorCells union CriticalCells union CenterCells;

param rad{Beams,HealtyCells union TumorCells union CriticalCells union CenterCells}; 
#fraction of radiation for each couple beam-region

param minr{CenterCells}; 
#minimal quantity of radiation necessary for treatment of central part of tumor cells

param r{TumorCells}; 
#quantity of radiation necessary for treatment of tumor cells

param maxr{CriticalCells}; 
#maximal quantity of radiation that healty cells must receive


var x{Beams} >=0; #beam qt (kilorad)


minimize healthy_anatomy: 
	sum{j in HealtyCells, i in Beams} rad[i,j]*x[i]; 
	#total radiation on healthy anatomy must be reduced

s.t. critical{j in CriticalCells}: 
	sum{i in Beams} rad[i,j]*x[i] <=maxr[j];
	#critical tissue must not radiated more then given kilorad

s.t. tumor_region{j in TumorCells}:
	sum{i in Beams} rad[i,j]*x[i] =r[j];
	#tumor region must be radiated with fixed quantity
 
s.t. center{j in CenterCells}:
	sum{i in Beams} rad[i,j]*x[i] >=minr[j];
	#the center of the tumor must be radiated at least with 6 kilorad

