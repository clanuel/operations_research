set R;

set F;

param avail {R};
param c{R};

param p{F};

param min_req{R,F} default 0;

param max_req{R,F} default 1;


var x{F} >=0;
var y{R,F} >=0;

maximize profit: sum{j in F}p[j]*x[j] 
					- sum{i in R, j in F}c[i]*y[i,j];
					
s.t. oil_availability{i in R}: sum{j in F}y[i,j] <=avail[i];
s.t. production{j in F}: sum{i in R} y[i,j]= x[j];
s.t. min_amount{i in R, j in F}: y[i,j] >= min_req[i,j]*x[j];
s.t. max_amount{i in R, j in F}: y[i,j] <= max_req[i,j]*x[j];