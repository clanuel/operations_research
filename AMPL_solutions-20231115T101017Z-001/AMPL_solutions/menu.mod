############# model file #############
set F; #food
set V; #vegetable
set I within F; # set of incompatible food

param p{F};   #cost
param c{F};   #calories

param calories;
param M;
param vegetable_fraction;



var x{i in F} >= 0;
var y{i in I} binary;


minimize total_cost: sum {i in F} p[i]*x[i];

subject to required_calories:
sum {i in F} c[i]*x[i] >= calories ;

subject to vegetables: sum{i in V}c[i]*x[i] 
 					>=vegetable_fraction*sum{i in F}c[i]*x[i];


subject to bigM{i in I}:
 	x[i] <= M*y[i];
 	
 subject to at_most_one: sum{i in I} y[i] <=1;
 	
 
